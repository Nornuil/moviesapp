package com.example.practicamvvm.data.remote

import com.example.practicamvvm.application_utils.AppConstants
import com.example.practicamvvm.data.model.MovieList
import com.example.practicamvvm.repository.WebService

class MovieDataSource(private val webService: WebService) {
    // creo los 3 metodos para obtener las 3 listas del servidor
    suspend fun getUpconmingMovie(): MovieList = webService.getUpcomingMovies(AppConstants.API_KEY)

    suspend fun getTopRatedMovies(): MovieList = webService.getTopRatedMovies(AppConstants.API_KEY)

    suspend fun getPopularMovies(): MovieList = webService.getPopularMovies(AppConstants.API_KEY)

}