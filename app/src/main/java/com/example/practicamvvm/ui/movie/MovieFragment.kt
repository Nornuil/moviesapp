package com.example.practicamvvm.ui.movie

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.example.practicamvvm.R
import com.example.practicamvvm.databinding.FragmentMovieBinding
import com.example.practicamvvm.core.Resource
import com.example.practicamvvm.data.model.Movie
import com.example.practicamvvm.data.remote.MovieDataSource
import com.example.practicamvvm.presentation.MovieViewModel
import com.example.practicamvvm.presentation.MovieViewModelFactory
import com.example.practicamvvm.repository.MovieRepositoryImpl
import com.example.practicamvvm.repository.RetrofitClient
import com.example.practicamvvm.ui.movie.adapters.MovieAdapter
import com.example.practicamvvm.ui.movie.adapters.concat.PopularConcatAdapter
import com.example.practicamvvm.ui.movie.adapters.concat.TopRatedConcatAdapter
import com.example.practicamvvm.ui.movie.adapters.concat.UpComingConcatAdapter


class MovieFragment : Fragment(R.layout.fragment_movie), MovieAdapter.OnMovieClickListener {

    private lateinit var binding: FragmentMovieBinding
    private val viewModel by viewModels<MovieViewModel> {
        MovieViewModelFactory(
            MovieRepositoryImpl(
                MovieDataSource(RetrofitClient.webService)
            )
        )
    }

    private lateinit var concatAdapter: ConcatAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieBinding.bind(view)

        concatAdapter = ConcatAdapter()

        viewModel.fetchMainScreenMovies().observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
//                    Log.d("LiveData","Loading...")
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressBar.visibility = View.VISIBLE
//                    Log.d("LiveData","Upcoming: ${result.data.first}")
//                    Log.d("LiveData2","TopRated: ${result.data.second}")
//                    Log.d("LiveData3","Popular: ${result.data.third}")
                    concatAdapter.apply {
                        addAdapter(
                            0,
                            UpComingConcatAdapter(
                                MovieAdapter(
                                    result.data.first.results,
                                    this@MovieFragment
                                )
                            )
                        )
                        addAdapter(
                            1,
                            TopRatedConcatAdapter(
                                MovieAdapter(
                                    result.data.second.results,
                                    this@MovieFragment
                                )
                            )
                        )
                        addAdapter(
                            2,
                            PopularConcatAdapter(
                                MovieAdapter(
                                    result.data.third.results,
                                    this@MovieFragment
                                )
                            )
                        )
                    }

                    binding.rvMovies.adapter = concatAdapter
                }
                is Resource.Failure -> {
//                    Log.d("Error","${result.exception}")
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })
    }

    override fun onMovieClick(movie: Movie) {
//        Log.d("Movie","onMovieClick: ${movie.original_tittle}")
        val action = MovieFragmentDirections.actionMovieFragmentToMovieDetailsFragment(
            movie.poster_path,
            movie.backdrop_path,
            movie.vote_average.toFloat(),
            movie.vote_count,
            movie.overview,
            movie.title,
            movie.original_language,
            movie.release_date
        )
        findNavController().navigate(action)
    }
}